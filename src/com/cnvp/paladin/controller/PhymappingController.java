package com.cnvp.paladin.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Phymapping;;

public class PhymappingController extends BaseController {
	
	public void index(){
		setAttr("page", Phymapping.dao.paginate(getParaToInt(0, 1), 10));
	}
	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ?-1 : getParaToInt("id");
            String name = getPara("name");
            String type = getPara("typ");
            String city = getPara("city");
            String environment = getPara("environment");
            String building    = getPara("building");
            int floor          = getPara("floor") == null ? -1 : getParaToInt("floor");
            String num_in_floor= getPara("num_in_floor");
            
            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (name != null)findSql += "name like '%" + name + "%'" + " and ";
            if (type != null)findSql += "type like '%" + type + "%'" + " and ";
            if (city != null)findSql += "city like '%" + city + "%'" + " and ";
            if (environment != null) findSql += "environment like '%" + environment + "%'" + " and ";
            if (building != null)findSql += "building like '%" + building + "%'" + " and ";
            if (floor != -1) findSql += "floor=" + floor + " and ";
            if (num_in_floor != null)findSql += "num_in_floor like '%" + num_in_floor + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Phymapping> data = Phymapping.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Phymapping> data = Phymapping.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Phymapping.class,"phymapping").save())
				redirect(getControllerKey());
				return;
		}
		Phymapping data = new Phymapping();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Phymapping.class,"phymapping").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Phymapping.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Phymapping.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Phymapping.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}