package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Equipment;

public class EquipmentController extends BaseController {
	
	public void index(){
		setAttr("page", Equipment.dao.paginate(getParaToInt(0, 1), 10));
	}
	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ? -1 : getParaToInt("id");
            String name = getPara("name");
            int room_id = getParaToInt("room_id") == null ? -1 : getParaToInt("room_id");
            String num_in_room = getPara("num_in_room");
            int panel_id    = getParaToInt("panel_id") == null ? -1 : getParaToInt("panel_id");
            int status      = getParaToInt("status") == null ? -1 : getParaToInt("status");
            String param1   = getPara("param1");
            String param2   = getPara("param2");
            String param3   = getPara("param3");
            String param4   = getPara("param4");
            String param5   = getPara("param5");

            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (room_id != -1) findSql += "room_id=" + room_id + " and ";
            if (num_in_room != null) findSql += "num_in_room like '%" + num_in_room + "%' and ";
            if (panel_id != -1) findSql += "panel_id=" + panel_id + " and ";
            if (status != -1) findSql += "status=" + status + " and ";
            if (name != null)findSql += "name like '%" + name + "%'" + " and ";
            if (param1 != null)findSql += "param1 like '%" + param1 + "%'" + " and ";
            if (param2 != null)findSql += "param2 like '%" + param2 + "%'" + " and ";
            if (param3 != null)findSql += "param3 like '%" + param3 + "%'" + " and ";
            if (param4 != null)findSql += "param4 like '%" + param4 + "%'" + " and ";
            if (param5 != null)findSql += "param5 like '%" + param5 + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Equipment> data = Equipment.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Equipment> data = Equipment.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Equipment.class,"equipment").save())
				redirect(getControllerKey());
				return;
		}
		Equipment data = new Equipment();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Equipment.class,"equipment").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Equipment.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Equipment.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Equipment.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}