package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Scene;

public class SceneController extends BaseController {
	
	public void index(){
		setAttr("page", Scene.dao.paginate(getParaToInt(0, 1), 10));
	}
	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ?-1 : getParaToInt("id");
            String name = getPara("name");
            String image_id = getPara("image_id");
            
            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (name != null)findSql += "name like '%" + name + "%'" + " and ";
            if (image_id != null)findSql += "image_id like '%" + image_id + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Scene> data = Scene.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Scene> data = Scene.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Scene.class, "scene").save())
				redirect(getControllerKey());
				return;
		}
		Scene data = new Scene();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Scene.class,"scene").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Scene.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Scene.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Scene.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}