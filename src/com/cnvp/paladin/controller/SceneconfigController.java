package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Sceneconfig;

public class SceneconfigController extends BaseController {
	
	public void index(){
		setAttr("page", Sceneconfig.dao.paginate(getParaToInt(0, 1), 10));
	}

	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ? -1 : getParaToInt("id");
            int scene_id = getParaToInt("scene_id") == null ? -1 : getParaToInt("scene_id");
            int equipment_id    = getParaToInt("equipment_id") == null ? -1 : getParaToInt("equipment_id");
            int status      = getParaToInt("status") == null ? -1 : getParaToInt("status");
            String param1   = getPara("param1");
            String param2   = getPara("param2");
            String param3   = getPara("param3");
            String param4   = getPara("param4");
            String param5   = getPara("param5");

            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (scene_id != -1) findSql += "scene_id =" + scene_id + " and ";
            if (equipment_id != -1) findSql += "equipment_id=" + equipment_id + " and ";
            if (status != -1) findSql += "status=" + status + " and ";
            if (param1 != null)findSql += "param1 like '%" + param1 + "%'" + " and ";
            if (param2 != null)findSql += "param2 like '%" + param2 + "%'" + " and ";
            if (param3 != null)findSql += "param3 like '%" + param3 + "%'" + " and ";
            if (param4 != null)findSql += "param4 like '%" + param4 + "%'" + " and ";
            if (param5 != null)findSql += "param5 like '%" + param5 + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Sceneconfig> data = Sceneconfig.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Sceneconfig> data = Sceneconfig.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Sceneconfig.class,"sceneconfig").save())
				redirect(getControllerKey());
				return;
		}
		Sceneconfig data = new Sceneconfig();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Sceneconfig.class, "sceneconfig").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Sceneconfig.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Sceneconfig.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Sceneconfig.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}