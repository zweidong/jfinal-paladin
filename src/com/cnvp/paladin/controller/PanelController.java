package com.cnvp.paladin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Panel;

public class PanelController extends BaseController {
	
	public void index(){
		setAttr("page", Panel.dao.paginate(getParaToInt(0, 1), 10));
	}
	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ?-1 : getParaToInt("id");
            String name = getPara("name");
            String type = getPara("typ");
            
            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (name != null)findSql += "name like '%" + name + "%'" + " and ";
            if (type != null)findSql += "type like '%" + type + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Panel> data = Panel.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Panel> data = Panel.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Panel.class, "panel").save())
				redirect(getControllerKey());
				return;
		}
		Panel data = new Panel();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Panel.class,"panel").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Panel.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Panel.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Panel.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}