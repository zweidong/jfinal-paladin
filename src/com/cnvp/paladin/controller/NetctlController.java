package com.cnvp.paladin.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cnvp.paladin.core.BaseController;
import com.cnvp.paladin.model.Netcontroller;

public class NetctlController extends BaseController {
	
	public void index(){
		setAttr("page", Netcontroller.dao.paginate(getParaToInt(0, 1), 10));
	}
	public void find() {
		Map<String, Object> json =  new HashMap<String, Object>();
		if(isPost()){
			int id      = getParaToInt("id") == null ?-1 : getParaToInt("id");
            String ip_address = getPara("ip_address");
			int port          = getParaToInt("port ") == null ?-1 : getParaToInt("port");
            String main       = getPara("main");
            String address    = getPara("address");
            String domain     = getPara("domain");
            String net_select = getPara("net_select");
            
            String findSql = "(1=1) and ";
            if (id != -1) findSql += "id=" + id + " and ";
            if (ip_address != null)findSql += "ip_address like '%" + ip_address + "%'" + " and ";
            if (port != -1) findSql += "port=" + port + " and ";
            if (main != null)findSql += "main like '%" + main + "%'" + " and ";
            if (address != null)findSql += "address like '%" + address + "%'" + " and ";
            if (domain != null) findSql += "domain like '%" + domain + "%'" + " and ";
            if (net_select != null)findSql += "net_select like '%" + net_select + "%'" + " and ";
            findSql += " (1=1);";
            
            List<Netcontroller> data = Netcontroller.dao.where(findSql);
            json.put("data", data);
		}
        renderJson(json);
	}
	public void getlist(){
		Map<String, Object> json =  new HashMap<String, Object>();
		List<Netcontroller> data = Netcontroller.dao.where("");
		json.put("data", data);
		renderJson(json);
	}
	
	public void create(){
		if(isPost()){
			if(getModel(Netcontroller.class, "netcontroller").save())
				redirect(getControllerKey());
				return;
		}
		Netcontroller data = new Netcontroller();
		setAttr("data", data);
		render("form.html");
	}

	public void update(){
		if(isPost()){
			if(getModel(Netcontroller.class,"netcontroller").set("id", getParaToInt()).update())
				redirect(getControllerKey());
				return;
		}
		setAttr("data", Netcontroller.dao.findById(getParaToInt()));
		render("form.html");
	}
	public void delete(){
		if (Netcontroller.dao.findById(getParaToInt()).delete()) 
			redirect(getControllerKey());
		else
			renderText("删除失败");
	}
	public void deleteAll(){
		Integer[] ids=getParaValuesToInt("id");
		for (Integer id : ids) {
			Netcontroller.dao.findById(id).delete();
		}
		redirect(getControllerKey());
	}
	
}