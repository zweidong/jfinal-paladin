/*
 Navicat Premium Data Transfer

 Source Server         : localmysql1
 Source Server Type    : MySQL
 Source Server Version : 50626
 Source Host           : localhost
 Source Database       : cjf

 Target Server Type    : MySQL
 Target Server Version : 50626
 File Encoding         : utf-8

 Date: 09/11/2015 21:58:31 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `cnvp_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_equipment`;
CREATE TABLE `cnvp_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `room_id` int(11) NOT NULL,
  `num_in_room` varchar(32) NOT NULL,
  `panel_id` int(11) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  `param1` varchar(512) DEFAULT NULL,
  `param2` varchar(512) DEFAULT NULL,
  `param3` varchar(512) DEFAULT NULL,
  `param4` varchar(512) DEFAULT NULL,
  `param5` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_equipment`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_equipment` VALUES ('1', '显示器', '1', '2', '1', '1', 'p1', 'p2', 'p3', 'p4', 'p5'), ('2', '水晶吊灯', '12', '12', '12', '12', '五色灯', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf'), ('3', '热水壶', '12', '23', '23', '23', 'asdfasdf', 'asdf', 'sdf', 'asdf', 'asdfasdf'), ('5', '吸顶灯', '12', '23', '434', '1', '阿斯顿发斯蒂芬', 'asdf', 'asdf', 'asdf', 'asdf'), ('6', '台灯', '12', '23-23', '123', '待机', 'asdfasdf', 'sdfdfsdfs', 'dfasdfsdf', 'df', 'asdfasdf');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_netcontroller`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_netcontroller`;
CREATE TABLE `cnvp_netcontroller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(128) NOT NULL,
  `port` int(32) NOT NULL,
  `main` varchar(512) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `domain` varchar(512) DEFAULT NULL,
  `net_select` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_netcontroller`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_netcontroller` VALUES ('1', '129.34.123.123', '123', 'nomain', '门厅', 'www.menting.net', 'do net_select'), ('2', '123.34.90.23', '123', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf'), ('3', '189.15.23.12', '123', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_panel`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_panel`;
CREATE TABLE `cnvp_panel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `type` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_panel`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_panel` VALUES ('1', '照明控制板', '墙面固定'), ('2', '采暖控制板', 'asdfasdfasdf');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_phymapping`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_phymapping`;
CREATE TABLE `cnvp_phymapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loc_id` int(11) NOT NULL,
  `phy_addr` varchar(512) DEFAULT NULL,
  `mac_addr` varchar(48) NOT NULL,
  `type` varchar(512) NOT NULL,
  `route` int(8) DEFAULT NULL,
  `tag` varchar(32) DEFAULT NULL,
  `describe` varchar(512) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_phymapping`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_phymapping` VALUES ('1', '1', '天花板', '89sd23sadf', '1', '1', '1', 'asdf', '1'), ('2', '12', 'sdfasdf', 'asdfasdf', 'asdfasdf', '1', '实线', 'asdfasdf', '工作');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_room`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_room`;
CREATE TABLE `cnvp_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `environment` varchar(512) DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `city` varchar(512) DEFAULT NULL,
  `building` varchar(512) DEFAULT NULL,
  `floor` int(8) DEFAULT NULL,
  `num_in_floor` varchar(32) DEFAULT NULL,
  `delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_room`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_room` VALUES ('1', '饭堂大厅', '居家', '客厅', '南京', '紫峰大厦', '45', '34', '0'), ('2', '荷花会客厅', '酒店', '会议室', '杭州', '杭州大厦', '10', '1023', null), ('4', 'asdfasdf', 'asdf', 'asdf', '天津', '明珠大厦', '12', '12-23', null);
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_scene`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_scene`;
CREATE TABLE `cnvp_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `image_id` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_scene`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_scene` VALUES ('1', '夜景模式', 'asdfasdf.jpg'), ('2', '星光模式', 'asdfsdf.jpg'), ('3', '晚霞满天', 'wanxiamantian.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sceneconfig`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sceneconfig`;
CREATE TABLE `cnvp_sceneconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scene_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  `param1` varchar(512) DEFAULT NULL,
  `param2` varchar(512) DEFAULT NULL,
  `param3` varchar(512) DEFAULT NULL,
  `param4` varchar(512) DEFAULT NULL,
  `param5` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sceneconfig`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sceneconfig` VALUES ('1', '123', '123', 'asdfsdfsd', 'fasdfsdfs', 'sdfsdfsd', 'fsdfsdf', 'sdfasdfsd', 'asdf');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_dept`;
CREATE TABLE `cnvp_sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `cname` varchar(200) NOT NULL COMMENT '名称',
  `desc` varchar(500) DEFAULT NULL COMMENT '描述',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sys_dept`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_dept` VALUES ('1', '杭州捷点', null, '0'), ('16', '电商', null, '1'), ('14', '技术部', null, '15'), ('15', '科技', null, '1'), ('8', '客服部', null, '15'), ('9', '商务部', null, '15'), ('17', 'A部门', null, '16'), ('18', 'B部门', null, '16');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_nav`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_nav`;
CREATE TABLE `cnvp_sys_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统id',
  `title` varchar(100) NOT NULL COMMENT '文字',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `url` varchar(100) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) NOT NULL DEFAULT 'mainFrame' COMMENT '目标',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `orderid` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `res_id` int(11) NOT NULL DEFAULT '0' COMMENT '资源Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='导航';

-- ----------------------------
--  Records of `cnvp_sys_nav`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_nav` VALUES ('1', '开始', 'icon-home', null, 'mainFrame', '0', '10', '88'), ('3', '系统', 'icon-cog', null, 'mainFrame', '0', '10', '89'), ('33', '组织结构', 'icon-sitemap', null, 'mainFrame', '3', '2', '48'), ('20', '模型生成', 'icon-code', 'Generator/model', 'mainFrame', '2', '10', '82'), ('17', '欢迎使用', 'icon-send', null, 'mainFrame', '1', '10', '78'), ('18', '个人资料', 'icon-user', null, 'mainFrame', '1', '10', '79'), ('19', '修改密码', 'icon-key', null, 'mainFrame', '1', '10', '80'), ('2', '开发', 'icon-codepen', null, 'mainFrame', '0', '10', '90'), ('15', '12312312', '312', null, 'mainFrame', '123', '10', '0'), ('21', '视图生成', 'icon-code', 'Generator/view', 'mainFrame', '2', '10', '82'), ('22', '控制器生成', 'icon-code', 'Generator/controller', 'mainFrame', '2', '10', '82'), ('23', '系统设置', 'icon-desktop', null, 'mainFrame', '3', '1', '3'), ('24', '导航菜单', 'icon-th-list', null, 'mainFrame', '3', '6', '73'), ('26', '文件管理', 'icon-file', null, 'mainFrame', '3', '7', '0'), ('27', '日志查询', 'icon-angle-double-right', null, 'mainFrame', '3', '9', '0'), ('32', '用户管理', 'icon-user', null, 'mainFrame', '3', '3', '54'), ('34', '角色管理', 'icon-group', null, 'mainFrame', '3', '4', '60'), ('35', '资源管理', 'icon-unlock-alt', null, 'mainFrame', '3', '5', '67'), ('36', '设备管理', 'icon-codepen', '/Equipment', 'mainFrame', '1', '10', '1'), ('37', '智能', 'icon-send', null, 'mainFrame', '0', '10', '105'), ('38', '设备管理', 'icon-cubes', null, 'mainFrame', '37', '2', '100'), ('39', '房间管理', 'icon-th', null, 'mainFrame', '37', '1', '106'), ('40', '物理设备管理', 'icon-gears', null, 'mainFrame', '37', '3', '111'), ('41', '面板管理', 'icon-credit-card', null, 'mainFrame', '37', '4', '116'), ('42', '网关管理', 'icon-retweet', null, 'mainFrame', '37', '5', '120'), ('43', '情景管理', 'icon-eye', null, 'mainFrame', '37', '6', '130'), ('44', '情景配置', 'icon-bar-chart-o', null, 'mainFrame', '37', '7', '130');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_res`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_res`;
CREATE TABLE `cnvp_sys_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统Id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  `cname` varchar(50) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '权限识别码',
  `code_route` varchar(500) DEFAULT NULL COMMENT '权限路径',
  `des` varchar(200) DEFAULT NULL COMMENT '描述',
  `ak` varchar(200) DEFAULT NULL COMMENT 'actionKey',
  `seq` int(11) DEFAULT '10' COMMENT '排序',
  `type` tinyint(2) DEFAULT NULL COMMENT '1、actionKey；',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sys_res`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_res` VALUES ('1', '0', '开始', 'start', 'start', '开始', null, '10', '0'), ('2', '0', '系统', 'system', 'system', null, null, '10', '0'), ('3', '2', '系统设置', 'config', 'system:config', null, '/System', '10', '1'), ('47', '2', '组织机构', 'dept', 'system:dept', null, null, '10', '0'), ('48', '47', '首页', 'index', 'system:dept:index', null, '/Dept', '10', '1'), ('49', '47', '抽取json数据', 'getlist', 'system:dept:getlist', null, '/Dept/getlist', '10', '1'), ('50', '47', '创建', 'create', 'system:dept:create', null, '/Dept/create', '10', '1'), ('51', '47', '更新', 'update', 'system:dept:update', null, '/Dept/update', '10', '1'), ('52', '47', '删除', 'delete', 'system:dept:delete', null, '/Dept/delete', '10', '1'), ('53', '2', '用户管理', 'user', 'system:user', null, null, '10', '0'), ('54', '53', '列表', 'index', 'system:user:index', null, '/User', '10', '1'), ('55', '53', '添加', 'create', 'system:user:create', null, '/User/create', '10', '1'), ('56', '53', '修改', 'update', 'system:user:update', null, '/User/update', '10', '1'), ('57', '53', '删除', 'delete', 'system:user:delete', null, '/User/delete', '10', '1'), ('58', '53', '批量删除', 'deleteall', 'system:user:deleteall', null, '/User/deleteAll', '10', '1'), ('59', '2', '角色管理', 'role', 'system:role', null, null, '10', '0'), ('60', '59', '列表', 'index', 'system:role:index', null, '/Role', '10', '1'), ('61', '59', '添加', 'create', 'system:role:create', null, '/Role/create', '10', '1'), ('62', '59', '更新', 'update', 'system:role:update', null, '/Role/update', '10', '1'), ('63', '59', '删除', 'delete', 'system:role:delete', null, '/Role/delete', '10', '1'), ('64', '59', '批量删除', 'deleteAll', 'system:role:deleteAll', null, '/Role/deleteAll', '10', '1'), ('65', '59', '抽取JSON数据', 'getlist', 'system:role:getlist', null, '/Role/getlist', '10', '1'), ('66', '2', '资源管理', 'res', 'system:res', null, null, '10', '0'), ('67', '66', '首页', 'index', 'system:res:index', null, '/Resource', '10', '1'), ('68', '66', '添加', 'create', 'system:res:create', null, '/Role/create', '10', '1'), ('69', '66', '更新', 'update', 'system:res:update', null, '/Resource/update', '10', '1'), ('70', '66', '删除', 'delete', 'system:res:delete', null, '/Resource/delete', '10', '1'), ('71', '66', '抽取JSON数据', 'getlist', 'system:res:getlist', null, '/Resource/getlist', '10', '1'), ('72', '2', '导航管理', 'sysnav', 'system:sysnav', null, null, '10', '0'), ('73', '72', '首页', 'index', 'system:sysnav:index', null, '/System/nav', '10', '1'), ('74', '72', '添加', 'create', 'system:sysnav:create', null, '/System/nav_create', '10', '1'), ('75', '72', '更新', 'update', 'system:sysnav:update', null, '/System/nav_update', '10', '1'), ('76', '72', '删除', 'delete', 'system:sysnav:delete', null, '/System/nav_delete', '10', '1'), ('77', '72', '保存排序', 'saveorder', 'system:sysnav:saveorder', null, '/System/save_order', '10', '1'), ('78', '1', '欢迎使用', 'welcome', 'start:welcome', null, '/welcome', '10', '1'), ('79', '1', '个人资料', 'profile', 'start:profile', null, '/profile', '10', '1'), ('80', '1', '修改密码', 'password', 'start:password', null, '/password', '10', '1'), ('81', '0', '开发', 'dev', 'dev', null, null, '10', '0'), ('82', '81', '首页', 'index', 'dev:index', null, '/Generator', '10', '1'), ('83', '81', '模型代码', 'model', 'dev:model', null, '/Generator/model_code', '10', '1'), ('84', '81', '控制器代码', 'controller', 'dev:controller', null, '/Generator/controller_code', '10', '1'), ('85', '81', '视图代码', 'view', 'dev:view', null, '/Generator/view_code', '10', '1'), ('86', '59', '配置资源权限', 'set_res', 'system:role:set_res', null, '/Role/set_res', '10', '1'), ('87', '0', '管理首页', 'frame', 'frame', null, '/', '0', '1'), ('88', '1', '头部导航', 'topnav', 'start:topnav', null, null, '0', '0'), ('89', '2', '头部导航', 'topnav', 'system:topnav', null, null, '0', '0'), ('90', '81', '头部导航', 'topnav', 'dev:topnav', null, null, '0', '0'), ('91', '0', '智能', 'intelligence', 'intelligence', null, null, '10', '0'), ('94', '91', '设备', 'equipment', 'intelligence:equipment', null, null, '10', '0'), ('93', '91', '房间', 'room', 'intelligence:room', null, null, '10', '0'), ('95', '91', '物理设备', 'phymapping', 'intelligence:phymapping', null, null, '10', '0'), ('96', '91', '面板', 'panel', 'intelligence:panel', null, null, '10', '0'), ('97', '91', '网关', 'netcontroller', 'intelligence:netcontroller', null, null, '10', '0'), ('98', '91', '情景', 'scene', 'intelligence:scene', null, null, '10', '0'), ('99', '91', '情景配置', 'sceneconfig', 'intelligence:sceneconfig', null, null, '10', '0'), ('100', '94', '列表', 'index', 'intelligence:equipment:index', null, '/Equipment', '10', '1'), ('101', '94', '添加', 'create', 'intelligence:equipment:create', null, '/Equipment/create', '10', '1'), ('102', '94', '修改', 'update', 'intelligence:equipment:update', null, '/Equipment/update', '10', '1'), ('103', '94', '删除', 'delete', 'intelligence:equipment:delete', null, '/Equipment/delete', '10', '1'), ('104', '94', '批量删除', 'deleteall', 'intelligence:equipment:deleteall', null, '/Equipment/deleteAll', '10', '1'), ('105', '91', '头部导航', 'topnav', 'intelligence:topnav', null, null, '10', '0'), ('106', '93', '列表', 'index', 'intelligence:room:index', null, '/Room', '10', '1'), ('107', '93', '添加', 'create', 'intelligence:room:create', null, '/Room/create', '10', '1'), ('108', '93', '修改', 'update', 'intelligence:room:update', null, '/Room/update', '10', '1'), ('109', '93', '删除', 'delete', 'intelligence:room:delete', null, '/Room/delete', '10', '1'), ('110', '93', '批量删除', 'deleteall', 'intelligence:room:deleteall', null, '/Room/deleteAll', '10', '1'), ('111', '95', '列表', 'index', 'intelligence:phymapping:index', null, '/Phymapping', '10', '1'), ('112', '95', '添加', 'create', 'intelligence:phymapping:create', null, '/Phymapping/create', '10', '1'), ('113', '95', '修改', 'update', 'intelligence:phymapping:update', null, '/Phymapping/update', '10', '1'), ('114', '95', '删除', 'delete', 'intelligence:phymapping:delete', null, '/Phymapping/delete', '10', '1'), ('115', '95', '批量删除', 'deleteall', 'intelligence:phymapping:deleteall', null, '/Phymapping/deleteAll', '10', '1'), ('116', '96', '列表', 'index', 'intelligence:panel:index', null, '/Panel', '10', '1'), ('117', '96', '添加', 'create', 'intelligence:panel:create', null, '/Panel/create', '10', '1'), ('118', '96', '删除', 'delete', 'intelligence:panel:delete', null, '/Panel/delete', '10', '1'), ('119', '96', '删除所有', 'deleteall', 'intelligence:panel:deleteall', null, '/Panel/deleteAll', '10', '1'), ('120', '97', '列表', 'index', 'intelligence:netcontroller:index', null, '/Netctl', '10', '1'), ('121', '97', '添加', 'create', 'intelligence:netcontroller:create', null, '/Netctl/create', '10', '1'), ('122', '97', '删除', 'delete', 'intelligence:netcontroller:delete', null, '/Netctl/delete', '10', '1'), ('123', '97', '修改', 'update', 'intelligence:netcontroller:update', null, '/Netctl/update', '10', '1'), ('124', '96', '修改', 'update', 'intelligence:panel:update', null, '/Panel/update', '10', '1'), ('125', '98', '列表', 'index', 'intelligence:scene:index', null, '/Scene', '10', '1'), ('126', '98', '添加', 'create', 'intelligence:scene:create', null, '/Scene/create', '10', '1'), ('127', '98', '修改', 'update', 'intelligence:scene:update', null, '/Scene/update', '10', '1'), ('128', '98', '删除', 'delete', 'intelligence:scene:delete', null, '/Scene/delete', '10', '1'), ('129', '98', '批量删除', 'deleteall', 'intelligence:scene:deleteall', null, '/Scene/deleteAll', '10', '1'), ('130', '99', '列表', 'index', 'intelligence:sceneconfig:index', null, '/Sceneconfig', '10', '1'), ('131', '99', '添加', 'create', 'intelligence:sceneconfig:create', null, '/Sceneconfig/create', '10', '1'), ('132', '99', '修改', 'update', 'intelligence:sceneconfig:update', null, '/Sceneconfig/update', '10', '1'), ('133', '99', '删除', 'delete', 'intelligence:sceneconfig:delete', null, '/Sceneconfig/delete', '10', '1'), ('134', '99', '批量删除', 'deleteall', 'intelligence:sceneconfig:deleteall', null, '/Sceneconfig/deleteAll', '10', '1');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_role`;
CREATE TABLE `cnvp_sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色Id',
  `cname` varchar(20) NOT NULL COMMENT '角色名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级Id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sys_role`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_role` VALUES ('1', '普通用户', '0'), ('4', '系统管理员', '0');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_role_res`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_role_res`;
CREATE TABLE `cnvp_sys_role_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `res_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1348 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sys_role_res`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_role_res` VALUES ('1322', '4', '86'), ('1321', '4', '85'), ('1320', '4', '84'), ('1319', '4', '83'), ('1318', '4', '82'), ('1317', '4', '81'), ('1316', '4', '80'), ('1315', '4', '79'), ('1314', '4', '78'), ('1313', '4', '77'), ('1312', '4', '76'), ('1311', '4', '75'), ('1310', '4', '74'), ('1309', '4', '73'), ('1308', '4', '72'), ('1307', '4', '71'), ('1306', '4', '70'), ('1305', '4', '69'), ('1304', '4', '68'), ('1303', '4', '67'), ('1302', '4', '66'), ('1301', '4', '65'), ('1300', '4', '64'), ('1299', '4', '63'), ('1298', '4', '62'), ('1297', '4', '61'), ('1296', '4', '60'), ('1295', '4', '59'), ('1294', '4', '58'), ('1293', '4', '57'), ('1292', '4', '56'), ('1291', '4', '55'), ('1290', '4', '54'), ('1289', '4', '53'), ('1288', '4', '52'), ('1287', '4', '51'), ('1286', '4', '50'), ('1285', '4', '49'), ('1284', '4', '48'), ('1283', '4', '47'), ('1282', '4', '3'), ('1281', '4', '2'), ('1280', '4', '1'), ('1329', '1', '87'), ('1333', '1', '88'), ('1324', '4', '87'), ('1330', '1', '78'), ('1331', '1', '79'), ('1334', '4', '91'), ('1335', '4', '94'), ('1336', '4', '93'), ('1337', '4', '95'), ('1338', '4', '96'), ('1339', '4', '97'), ('1340', '4', '98'), ('1341', '4', '99'), ('1342', '4', '100'), ('1343', '4', '101'), ('1344', '4', '102'), ('1345', '4', '103'), ('1346', '4', '104'), ('1347', '4', '105');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_user`;
CREATE TABLE `cnvp_sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账号',
  `account` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `sex` enum('男','女') DEFAULT NULL COMMENT '性别',
  `cname` varchar(10) DEFAULT NULL COMMENT '中文名',
  `ename` varchar(50) DEFAULT NULL COMMENT '英文名',
  `dept_id` int(11) DEFAULT NULL COMMENT '所在部门',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `flg` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `last_login_time` bigint(13) DEFAULT NULL COMMENT '最后登录时间',
  `create_time` bigint(13) NOT NULL COMMENT '创建时间',
  `create_user_id` int(11) NOT NULL COMMENT '创建者',
  `update_time` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `update_user_id` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
--  Records of `cnvp_sys_user`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_user` VALUES ('1', 'superadmin', '63a9f0ea7bb98050796b649e85481845', '男', '超级管理员-章宵', 'michael.z', '14', '15990061612', '1', null, '60', '1413779275', '0', '1', '1425458171011', '1'), ('9', '200057', 'a8f5f167f44f4964e6c998dee827110c', '男', '章宵', 'Michael', '14', '15990061612', '1', null, '0', null, '1425707342830', '1', '1425783657455', '1'), ('14', 'tester', 'f5d1278e8109edd94e1e4197e04873b9', null, 'tester', 'tester', '1', null, '1', 'tester', '0', null, '1441897818366', '1', null, null), ('13', 'testuser1', '63a9f0ea7bb98050796b649e85481845', null, 'asdf', 'asdf', '1', null, '1', 'asdf', '0', null, '1441896820230', '1', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_sys_user_role`;
CREATE TABLE `cnvp_sys_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cnvp_sys_user_role`
-- ----------------------------
BEGIN;
INSERT INTO `cnvp_sys_user_role` VALUES ('1', '1'), ('1', '4'), ('4', '1'), ('9', '1'), ('10', '1'), ('10', '4'), ('13', '1'), ('14', '1');
COMMIT;

-- ----------------------------
--  Table structure for `cnvp_user_room`
-- ----------------------------
DROP TABLE IF EXISTS `cnvp_user_room`;
CREATE TABLE `cnvp_user_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `delete` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
